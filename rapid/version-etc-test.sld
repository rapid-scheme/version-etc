;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid version-etc-test)
  (export run-tests)
  (import (scheme base)
	  (rapid test)
	  (rapid version-etc))
  (begin
    (define (run-tests)
    
      (test-begin "Print --version and bug-reporting information in a consistent format")

      (test-equal
	  "rapid-scheme (Rapid Scheme) 0.1.0\n\
           Copyright © 2017 Marc Nieper-Wißkirchen\n\
           License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.\n\
           This is free software: you are free to change and redistribute it.\n\
           There is NO WARRANTY, to the extent permitted by law.\n"
	(let ((port (open-output-string)))
	  (parameterize ((current-output-port port)
			 (version-etc-copyright "Copyright © 2017 Marc Nieper-Wißkirchen"))
	    (version-etc "rapid-scheme" "Rapid Scheme" "0.1.0")
	    (get-output-string port))))

      (test-equal
	  "Email bug reports to: marc@rapid-scheme.org\n"
	(let ((port (open-output-string)))
	  (emit-bug-reporting-address "marc@rapid-scheme.org" port)
	  (get-output-string port)))
      
      (test-end))))
